subliminal (2.2.0-2) UNRELEASED; urgency=medium

  * Team upload.
  * Updating watch file to version 4
  * /dsubliminal.1: Add Manual-Page to packing

 -- Kaua Vinicius <kauaponte@hotmail.com>  Thu, 19 Dec 2024 11:55:40 -0300

subliminal (2.2.0-1) unstable; urgency=medium

  * Team Upload
  * New upstream version 2.2.0 (LP: #2046672)
  * Standards-Version: 4.7.0 (routine-update)
  * Update (build-)dependencies:
    - python3-appdirs (Closes: #1068012)
    - python3-pysrt
    - python3-six
    - python3-tz
    + pybuild-plugin-pyproject
    + python3-click-option-group
    + python3-platformdirs
    + python3-srt
    + python3-tomli
    + python3-pysubs2

 -- Alexandre Detiste <tchet@debian.org>  Thu, 04 Jul 2024 22:37:08 +0200

subliminal (2.1.0-3) unstable; urgency=medium

  * Team upload
  * Drop nautilus extension: it uses GTK3 but Nautilus 43 uses GTK4

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 01 Sep 2022 08:45:50 -0400

subliminal (2.1.0-2) unstable; urgency=medium

  * Update standards version to 4.5.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 26 May 2022 17:21:13 +0100

subliminal (2.1.0-1) unstable; urgency=medium

  * Team upload.
  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Håvard Flaget Aasen ]
  * New upstream version 2.1.0 Closes: #961338
  * Rebase patches
  * d/patches: Update 0001-Examples-directory.patch Closes: #955125
  * d/control:
    - Update maintainer field to Debian Python Team
    - Update Vcs-* fields with new Debian Python Team layout
    - Bump debhelper to 13
    - Add Rules-Requires-Root: no
  * d/copyright: Update copyright year
  * d/rules: Remove testsuite override
    source from PyPi does not include tests
  * d/salsa-ci.yml: Add Salsa CI

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Tue, 22 Sep 2020 20:20:30 +0200

subliminal (2.0.5-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Håvard Flaget Aasen ]
  * Bump debhelper to 12
  * subliminal-nautilus depends on python3-nautilus. Closes: #942779, #938575

 -- Sandro Tosi <morph@debian.org>  Sun, 19 Jan 2020 16:06:23 -0500

subliminal (2.0.5-2) unstable; urgency=medium

  * Team upload.
  * Rebuilt with python3-guessit (>= 2.0.1).

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Sep 2019 01:22:31 +0200

subliminal (2.0.5-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Team upload.
  * Removed Python 2 support (Closes: #938575).
  * New upstream release (Closes: #851535, #850098).
  * Fix installing files.
  * Reorder d/control in a more standard way.
  * Add python3-fix.patch.
  * Add override_dh_auto_install.
  * No need to override_dh_installman:.
  * Add fix-unexpected-indent.patch.
  * Removed help2man hack.
  * Ran wrap-and-sort, add python3:Depends to Packages: subliminal,
    call dh_python3 to fix the shebang.
  * Fix runtime and build depends. Note that we need python3-guessit
    (>= 2.0.1), but that's not yet in Debian.
  * Standards-Version bump to 4.4.0.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Sep 2019 23:59:15 +0200

subliminal (1.1.1-2) unstable; urgency=medium

  * Section is "video" for the subliminal binary package (Closes: #818333)
  * Ship Nautilus extension as subliminal-nautilus (Closes: #821455)
  * Bump Standards-Version to 3.9.8 (no changes)

 -- Etienne Millon <me@emillon.org>  Sat, 30 Apr 2016 19:29:54 +0200

subliminal (1.1.1-1) unstable; urgency=medium

  * New upstream release (Closes: #812211, #773381, #814848, #774064)
  * debian/watch: use pypi redirector
  * Bump Standard-Version to 3.9.6 (no changes)
  * Use a https URI for Vcs-Git

 -- Etienne Millon <me@emillon.org>  Wed, 26 Aug 2015 22:28:46 +0200

subliminal (0.7.4-1) unstable; urgency=low

  * Initial release. (Closes: #729781)

 -- Etienne Millon <me@emillon.org>  Sat, 16 Nov 2013 14:25:31 +0100
